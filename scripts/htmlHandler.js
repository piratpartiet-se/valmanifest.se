$(function () {
    $("#outerCard").click(function (e) {
        var showCard = parseInt($(e.currentTarget).css('margin-top')) === -200;
        var marginTop = showCard ? '-=' + (e.currentTarget.clientHeight - 200 + 10) + 'px' : "-200px";

        window.location.hash = window.location.hash.split('|')[0] + (showCard ? '|show' : '');
        $('#outerCard').animate({
            marginTop: marginTop
        }, 250);
    });

    $("#mainCanvas").on('nocircleclick', function (e) {
        $('#outerCard').animate({
            marginTop: '-200px'
        }, 250);
    });

    $('#mainCanvas').on('circleclick minimapclick', function (e, data) {
        var marginTop = '-200px';
        var showCard = data.extendedInfo
            && data.extendedInfo.showCard
            && data.extendedInfo.showCard === 'show';
        if (data.current.id === data.previous.id) { // same circle
            $('#title').html(data.current.title);
            $('#contents').html(data.current.text);
            showCard = showCard || parseInt($('#outerCard').css('margin-top')) === -200;
            marginTop = showCard
                ? parseInt($('#outerCard').css('margin-top')) === 0  // card is below the canvas, i.e. never shown before
                    ? -10 - $('#outerCard')[0].clientHeight
                    : '-=' + ($('#outerCard')[0].clientHeight - 200 + 10) + 'px'
                : "-200px";

            window.location.hash = data.current.id + (showCard ? '|show' : '');
            $('#outerCard').animate({
                marginTop: marginTop
            }, 250);
        } else {
            if (parseInt($('#outerCard').css('margin-top')) === 0) { // No card shown at the moment
                $('#title').html(data.current.title);
                $('#contents').html(data.current.text);
                marginTop = showCard
                    ? -10 - $('#outerCard')[0].clientHeight // card is below the canvas, i.e. never shown before
                    : "-200px";

            }
            $('#outerCard') // switching circle
                .animate({ marginTop: '0' }, 250) // move down
                .queue(function () { // Set new info
                    $('#title').html(data.current.title);
                    $('#contents').html(data.current.text);
                    $(this).dequeue();
                })
                .delay((Math.abs(data.current.level - data.previous.level) <= 1 ? 0 : Math.abs(data.current.level - data.previous.level) - 1) * 500) // wait on movement animation
                .animate({ marginTop: marginTop }, 250) // move up card
                .queue(function () { // end with setting url hash for reference
                    window.location.hash = (data.current.id !== 'root' ? data.current.id : '') + (showCard ? '|show' : '');
                    $(this).dequeue();
                });
        }
    });
});