var CanvasHandler = (function () {

    var circles = [],
        offset,
        animateZoomPosition = 1,
        zoom = 1,
        mousePoint;

    function clear(ctx) {
        ctx.beginPath();
        ctx.rect(0, 0, ctx.canvas.width, ctx.canvas.height );
        ctx.fillStyle = settings.colors.background;
        ctx.fill();
    }

    function getWorldCursorPosition(point) {
        return point.subtract(offset).divide(animateZoomPosition / settings.generalScale);
    }

    function createCircles(json) {
        var circle = addCircle(json.id, json.title, json.text, json.color);
        if (json.children) {
            json.children.forEach(function (child) {
                circle.attach(createCircles(child));
            });
        }
        return circle;
    }

    function addCircle(id, title, text, color) {
        var circle = new Circle(id, title, text, color);
        circles.push(circle);
        return circle;
    }

    function CanvasHandler(c, title, contents, json) {
        var me = this,
            titleElement,
            textElement,
            canvas = c[0],
            ctx = canvas.getContext('2d'),
            center,
            boundingClientRect,
            rootCircle,
            overCircle,
            selectedCircle,
            latestSelectedCircle,
            animateProgress,
            animationStart,
            animationOrigin,
            animationDestination,
            animationPosition,
            animateZoomOrigin,
            animateZoomDestination,
            inAnimation = false,
            fontLoaded = false,
            image = null;

        // Private functions
        function triggerAnimation(deltaLevel) {
            animateZoomOrigin = animateZoomPosition;
            animateZoomDestination = settings.zoom[selectedCircle.level];
            animationDestination = selectedCircle.getPosition().divide(settings.generalScale);
            animationOrigin = animationPosition = latestSelectedCircle.getPosition().divide(settings.generalScale);
            animationStart = Date.now();
            inAnimation = true;
            window.requestAnimationFrame(function () { animateSelection(500 * (deltaLevel > 0 ? deltaLevel : 1), Math.sign(animateZoomOrigin - animateZoomDestination)); });
        }

        function animateSelection(animationTime, direction) {
            var time = Date.now() - animationStart;
            animateZoomPosition = animateZoomOrigin + (animateZoomDestination - animateZoomOrigin) * (direction === -1 ? EasingFunctions.easeInQuad(time / animationTime) : EasingFunctions.easeOutCubic(time / animationTime));
            animationPosition = animationOrigin.subtract(animationOrigin.getDelta(animationDestination).multiply(EasingFunctions.easeOutQuad(time / animationTime)));
            if (time < animationTime) {
                window.requestAnimationFrame(function () { animateSelection(animationTime, direction); });
            } else {
                animateZoomPosition = animateZoomDestination;
                animationPosition = animationDestination;
                inAnimation = false;
                miniMap.visible = selectedCircle.level >= 3;
            }
            render();
        }

        var miniMap = {
            visible: false,
            height: 40,
            x: 86,
            y: 42,
            render: function (ctx) {
                if (!this.visible)
                    return;

                ctx.fillStyle = '#ffffff';
                ctx.strokeStyle = '#000000';
                ctx.lineWidth = 4;

                var largeDiameter = 40;
                var smallDiameter = 20;

                // connecting line
                ctx.beginPath();
                ctx.moveTo(this.x - 65, this.y);
                ctx.lineTo(this.x + 65, this.y);
                ctx.stroke();

                // center circle
                ctx.beginPath();
                ctx.arc(this.x, this.y, largeDiameter, 0, 2 * Math.PI);
                ctx.fill();
                ctx.stroke();

                // left circle
                ctx.lineWidth = 2;
                ctx.beginPath();
                ctx.arc(this.x - 65, this.y, smallDiameter, 0, 2 * Math.PI);
                ctx.fill();
                ctx.stroke();

                // right side
                ctx.beginPath();
                ctx.arc(this.x + 65, this.y, smallDiameter, 0, 2 * Math.PI);
                ctx.fill();
                ctx.stroke();

                ctx.strokeStyle = '#ffffff';
                ctx.lineWidth = 5;
                ctx.fillStyle = '#000000';
                ctx.font = '35px ' + settings.font;
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                ctx.strokeText('Tillbaka', this.x, this.y);
                ctx.fillText('Tillbaka', this.x, this.y);
            },
            hitTest: function (point) {
                if (!this.visible)
                    return false;
                return point.x <= this.x + 65 + 20 && point.y <= this.y + 40;
            },
            click: function (e) {
                if (!this.visible)
                    return;
                latestSelectedCircle = selectedCircle;
                selectedCircle = rootCircle;
                c.trigger('minimapclick', [{ current: selectedCircle, previous: latestSelectedCircle }]);
                triggerAnimation(Math.abs(latestSelectedCircle.level - selectedCircle.level));
            }
        };

        var logo = {
            top: 10,
            offsetRight: 110,
            width: 102.5,
            height: 108,
            render: function (ctx) {
                try {
                    if (settings.logotype && image === null) {
                        image = new Image();
                        image.onload = function () {
                            ctx.drawImage(image, boundingClientRect.width - this.offsetRight, this.top, this.width, this.height);
                        };
                        image.src = settings.logotype;
                    } else if (image.complete) {
                        ctx.drawImage(image, boundingClientRect.width - this.offsetRight, this.top, this.width, this.height);
                    }
                } catch (e) {
                    alert('Unable to load svg image ' + settings.logotype);
                    console.log('Unable to load svg image ' + settings.logotype);
                }
            },
            hitTest: function (point) {
                return point.x >= boundingClientRect.width - this.offsetRight && point.y <= this.top + this.height;
            },
            click: function (e) {
                if (settings.logotypeLink) {
                    document.location = settings.logotypeLink;
                }
            }
        };

        function render() {
            offset = center.getDelta(animationPosition.multiply(animateZoomPosition));

            clear(ctx);

            ctx.save();
            ctx.setTransform(animateZoomPosition / settings.generalScale, 0, 0, animateZoomPosition / settings.generalScale, offset.x, offset.y);
            rootCircle.render(ctx);
            ctx.restore();
            miniMap.render(ctx);
            logo.render(ctx);
        }

        function setSize() {
            canvas.setAttribute('width', boundingClientRect.width);
            canvas.setAttribute('height', boundingClientRect.height); // window.getComputedStyle(c, null).getPropertyValue("height"));
            center = new Point(canvas.width / 2, canvas.height / 2);
        }

        // Check all circles if the mouse cursor is over any of them, use the first found.
        function doHitTest(e) {
            var canvasCoord = new Point(e.pageX - boundingClientRect.left, e.pageY - boundingClientRect.top);
            mousePoint = getWorldCursorPosition(canvasCoord);
            for (var i = 0; i < circles.length; i++) {
                overCircle = circles[i].doHitTest(mousePoint);
                if (overCircle !== undefined)
                    break;
            }
            if (overCircle !== undefined) {
                canvas.style.cursor = 'pointer';
            } else {
                canvas.style.cursor = 'initial';
            }

            // check if the cursor is over the back button or the logo
            if (miniMap.hitTest(canvasCoord) || logo.hitTest(canvasCoord)) {
                canvas.style.cursor = 'pointer';
            }
        }

        function waitForFontToLoad(callback) {
            var testLine = '#�%&/(WwiI';
            ctx.font = '100px ' + settings.font;
            var width1 = ctx.measureText(testLine).width;
            ctx.font = '100px serif';
            var width2 = ctx.measureText(testLine).width;
            if (width1 === width2) {
                setTimeout(function () {
                    waitForFontToLoad(callback);
                }, 10);
            } else {
                fontLoaded = true;
                callback();
            }
        }

        // public functions
        me.resize = function () {
            if (!fontLoaded) {
                waitForFontToLoad(me.resize);
            } else {
                var vPad = parseInt(c.css('border-top-width'));
                var hPad = parseInt(c.css('border-left-width'));
                boundingClientRect = {
                    top: vPad,
                    left: hPad,
                    x: hPad,
                    y: vPad,
                    width: c.width(),
                    height: c.height(),
                    bottom: c.height(),
                    right: c.width()
                };
                canvas.width = boundingClientRect.width;
                canvas.height = boundingClientRect.height;//c.getBoundingClientRect();

                setSize();
                render(ctx.canvas.width < ctx.canvas.height ? ctx.canvas.width : ctx.canvas.height);
            }
        };

        // Private Event handlers
        function onMouseMove(e) {
            doHitTest(e);
        }

        function onClick(e) {
            var canvasCoord = new Point(e.pageX - boundingClientRect.left, e.pageY - boundingClientRect.top);
            if (miniMap.hitTest(canvasCoord)) {
                miniMap.click(e);
            } else if (logo.hitTest(canvasCoord)) {
                logo.click(e);
            } else {
                if (overCircle !== undefined) {
                    latestSelectedCircle = selectedCircle;
                    selectedCircle = overCircle;
                    c.trigger('circleclick', [{ current: selectedCircle, previous: latestSelectedCircle }]);
                    triggerAnimation(Math.abs(latestSelectedCircle.level - selectedCircle.level));
                } else {
                    c.trigger('nocircleclick');
                }
            }
        }

        // Attach external events
        canvas.onmousemove = onMouseMove;
        canvas.onclick = onClick;

        // Set animation start values
        animateZoomPosition = 1;
        animationPosition = new Point(0, 0);

        // Prepare to start everything
        titleElement = title;
        textElement = contents;
        latestSelectedCircle = selectedCircle = rootCircle = createCircles(json);
        rootCircle.setLevels();
        rootCircle.setRenderingAttributes();

        // Trigger a resize -> render and we're running!
        me.resize();

        var waitForRender = function (callback) {
            if (center === undefined) {
                setTimeout(function () {
                    waitForRender(callback);
                }, 10);
            } else {
                callback();
            }
        }

        waitForRender(function () {

            // Find the selected circle
            var windowHash = decodeURI(window.location.hash).split('|');
            var showCard = windowHash[1] === 'show';
            selectedCircle = circles.find(function (circle) {
                return '#' + circle.id === windowHash[0];
            }) || rootCircle;

            setTimeout(function () { // Make sure to wait a little if there is a sub-circle selected from the url.
                triggerAnimation(Math.abs(selectedCircle.level - rootCircle.level));
                c.trigger('circleclick', [{ current: selectedCircle, previous: rootCircle, extendedInfo: { showCard: showCard ? 'show' : 'hide' } }]);
            }, selectedCircle !== rootCircle ? 500 : 0);
        });
    }

    return CanvasHandler;
}());

$(function () {

    var xhr = new XMLHttpRequest();
    xhr.open('GET', window.location.pathname + 'data/config.json', true);
    xhr.responseType = 'json';
    xhr.onload = function () {
        if (xhr.status !== 200) {
            alert('Something went wrong: ' + xhr.status);
            console.log(xhr.response);
            return;
        }
        // get the settings
        settings = new Settings(xhr.response.settings);
        canvas = new CanvasHandler($('#mainCanvas'), $('#title'), $('#contents'), xhr.response.root);
        window.addEventListener('orientationchange', canvas.resize, false);
    };
    xhr.send();


});

$(window).resize(function () {
    canvas.resize();
});
