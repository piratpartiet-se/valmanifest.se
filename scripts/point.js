var Point = (function () {
    function Point(x, y) {
        var me = this;
        me.x = x;
        me.y = y;

        me.getDelta = function (point) {
            return new Point(me.x - point.x, me.y - point.y);
        };

        me.add = function (pointOrX, y) {
            if (typeof pointOrX === 'number' && typeof y === 'number') 
                return new Point(me.x + pointOrX, me.y + y);
            if(typeof pointOrX === 'object')
                return new Point(me.x + point.x, me.y + point.y);
            throw 'Unable to add ' + pointOrX + ' to a Point object';
        };

        me.subtract = function (point) {
            return new Point(me.x - point.x, me.y - point.y);
        };

        me.multiply = function (multiplier) {
            return new Point(me.x * multiplier, me.y * multiplier);
        };

        me.divide = function (divisor) {
            return new Point(me.x / divisor, me.y / divisor);
        };
    }
    return Point;
}());