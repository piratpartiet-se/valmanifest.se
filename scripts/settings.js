var Settings = (function () {
    function Settings(json) {
        var me = this;

        me.zoom = [];
        me.radius = [];
        me.angleType = [];
        me.angleDegrees = [];
        json.levels.forEach(function (level) {
            me.zoom[level.level] = level.zoom;
            me.radius[level.level] = level.radius;
            me.angleType[level.level] = level.angleType;
            me.angleDegrees[level.level] = level.angleDegrees;
        });

        me.colors = {
            background: json.colors.background,
            border: json.colors.border,
            fill: json.colors.fill,
            text: json.colors.text
        };

        Object.defineProperties(me, {
            "generalScale": { value: json.generalScale || 1, writable: false }
        });

        Object.defineProperties(me, {
            "lineWidthPercentage": { value: json.lineWidthPercentage || 0.07, writable: false }
        });

        Object.defineProperties(me, {
            "font": { value: json.font || 'LatoWeb', writable: false }
        });

        Object.defineProperties(me, {
            "fontSizePercentage": { value: json.fontSizePercentage || 0.1, writable: false }
        });

        Object.defineProperties(me, {
            "circleDistancePercentage": { value: json.circleDistancePercentage || 0.1, writable: false }
        });

        Object.defineProperties(me, {
            "logotype": { value: json.logotype || undefined, writable: false }
        });

        Object.defineProperties(me, {
            "logotypeLink": { value: json.logotypeLink || undefined, writable: false }
        });
    }

    return Settings;
}());