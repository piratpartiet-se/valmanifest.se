var Circle = (function () {

    function wrapText(ctx, text, x, y, maxWidth, lineHeight) {
        var words = text.split(' '),
            line = '',
            output = [],
            totalHeight = 0;

        for (var n = 0; n < words.length; n++) {
            var testLine = line + words[n] + ' ';
            var metrics = ctx.measureText(testLine.trim());
            var testWidth = metrics.width;
            if (testWidth > maxWidth) {
                const shy = '&shy;';
                var shyIndex = testLine.lastIndexOf(shy);
                if (shyIndex > -1) { // we do have a shy, let's cut at the last one and add a hyphen
                    line = line + words[n] + ' ';
                    output.push({ text: line.substring(0, shyIndex) + '-', x: x, y: y }); // add a hyphen
                    line = line.substring(shyIndex + shy.length);
                    y += lineHeight;
                    totalHeight += lineHeight;
                } else if(n > 0) {
                    output.push({ text: line, x: x, y: y });
                    line = words[n] + ' ';
                    y += lineHeight;
                    totalHeight += lineHeight;
                } else {
                    line = testLine; // Oh, well.. The word is just too long. Has to be handled with soft hyphen, &shy;, manually.
                }
            } else {
                line = testLine;
            }
        }
        output.push({ text: line.trim(), x: x, y: y });

        output.forEach(function (item) {
            ctx.fillText(item.text, item.x, item.y - totalHeight / 2);
        });
    }

    function Circle(id, title, text, color) {

        var me = this,
            attached = [],
            parent = null,
            position,
            radius,
            lineWidth,
            scale;

        me.getPosition = function () {
            return position;
        };

        me.isWithinCircle = function (point) {
            return Math.hypot(point.x - position.x, point.y - position.y) <= settings.radius[me.level];
        };

        me.doHitTest = function (point) {
            if (me.isWithinCircle(point)) {
                return me;
            }
        };

        me.attach = function (circle) {
            attached.push(circle);
            circle.setParent(me);
        };

        me.setParent = function (circle) {
            parent = circle;
        };

        me.getParent = function () {
            return parent;
        };

        me.setLevels = function () {
            updateLevel();
            attached.forEach(function (child) { child.setLevels(); });
        };

        me.getColor = function () {
            return me.color || me.getParent() && me.getParent().getColor() || settings.colors.fill;
        };

        me.setRenderingAttributes = function (origin, parentAngle) {
            parentAngle = parentAngle || 0;
            position = origin || new Point(0, 0);

            lineWidth = settings.radius[me.level] * settings.lineWidthPercentage / 100; // Set linewidth as a percentage of the radius
            radius = settings.radius[me.level] - lineWidth / 2;

            var spreadAngle = 0;
            switch (settings.angleType[me.level]) {
                case 'total':
                    spreadAngle = settings.angleDegrees[me.level] / 180 * Math.PI / (attached.length - (me.level === 1 ? 0 : 1));
                    break;
                case 'separation':
                    spreadAngle = settings.angleDegrees[me.level] / 180 * Math.PI;
                    break;
                default:
                    throw 'Unknown angle type: \'' + settings.angleType[me.level] + '\' in level ' + me.level;
            }
            var startAngle = parentAngle - spreadAngle * (attached.length - (me.level === 1 ? 0 : 1)) / 2;
            var childAngle = startAngle;

            attached.forEach(function (child) {
                var childOrigin = position.add(
                    Math.cos(childAngle) * (settings.radius[me.level] + settings.radius[child.level]) * (100 + settings.circleDistancePercentage) / 100,
                    Math.sin(childAngle) * (settings.radius[me.level] + settings.radius[child.level]) * (100 + settings.circleDistancePercentage) / 100);

                child.setRenderingAttributes(childOrigin, childAngle);
                childAngle += spreadAngle;
            });
        };

        me.render = function (ctx) {
            attached.forEach(function (attached) {
                ctx.beginPath();
                ctx.moveTo(position.x, position.y);
                var attachedPosition = attached.getPosition();
                ctx.lineTo(attachedPosition.x, attachedPosition.y);
                ctx.lineWidth = lineWidth;
                ctx.strokeStyle = settings.colors.border;
                ctx.stroke();

                attached.render(ctx);
            });

            ctx.beginPath();
            ctx.arc(position.x, position.y, radius, 0, 2 * Math.PI);
            ctx.fillStyle = me.getColor();
            ctx.fill();
            ctx.lineWidth = lineWidth;
            ctx.strokeStyle = settings.colors.border;
            ctx.stroke();

            var fontSize = radius * settings.fontSizePercentage / 100;
            ctx.font = fontSize + 'px Lato';
            ctx.fillStyle = settings.colors.text;
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            wrapText(ctx, stripHtml(title), position.x, position.y, radius * 2 - lineWidth, fontSize);

        };

        function stripHtml(html) {
            var regex = /(<([^>]+)>)/ig;

            return html.replace(regex, '');
        }

        // Private functions
        function updateLevel() {
            Object.defineProperties(me, {
                "level": { value: parent !== null ? parent.level + 1 : 1, writable: false, configurable: true }
            });
        }


        // Define properties
        Object.defineProperties(me, {
            "id": { value: id, writable: false }
        }); // id

        Object.defineProperties(me, {
            "title": { value: title, writable: false }
        }); // title

        Object.defineProperties(me, {
            "text": { value: text, writable: false }
        }); // text

        Object.defineProperties(me, {
            "level": { value: 1, writable: false, configurable: true }
        }); // level

        Object.defineProperties(me, {
            "color": { value: color, writable: false }
        }); // text
    }

    return Circle;
}());