# Valmanifest.se

Det grafiska vamanifest som vi använde oss av i riksdagsvalet 2018

Notera att filen upload.aspx var en nödlösning som jag gjorde så att Mattias Rubenson kunde ladda upp förändringar på config.json på min IIS utan assistans från mig. Den ska inte med i installationen, utan finns bara med för historik.